<?php echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" 
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
<title></title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <link rel="Shortcut Icon" type="image/x-icon" href="favicon.ico" />
  <meta name="generator" content="Drupal" />  
  <base href="<?php global $base_url; print $base_url;?>" />
  <style type="text/css" media="all">@import "themes/meta/css/maintenance.css";</style>
  <!--[if IE 6]>
  <script type="text/javascript" src="themes/meta/js/pngfix.js"></script>
  <![endif]-->
</head>
<body>

<div id="message-box">
	<?php if ($site_name != ""): ?>
    <h2><?php print $site_name ?><br />503 - Service Unavailable</h2>
    <?php else: ?>
    <h2>503 - Service Unavailable<br />Sorry For The Inconvenience</h2>
  <?php endif; ?>
	<div id="message"><?php print $content ?></div>
</div>

<div>
  <img src="themes/meta/css/images/maintenance-drupalicon.png" width="359" height="460" alt="Drupal Logo" id="drupal-logo" />
</div>

</body>
</html>
