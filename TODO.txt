
|   Bugs I've Noticed:
 `-------------------------------------------------------------------------+

GENERAL - The Meta theme uses a lot of absolute positioned DIV tags in it's
design and XHTML/CSS coding. This provides a powerful way to accomplish the
complex presentation and navigation tabs in the header. Absolute positioned
DIV tags are also used for the content area vs. floats. This has advantages
too but one big disadvantage, AP DIVs cannot be cleared like floated DIVs can
be. For this reason, the content area and it left, middle, right elements,
have a minimal height specified of 1000 pixels. In some cases, where the
navigation is long on the right, it will break past the footer area. Each
person's site is unique and for most the 1000 pixel minimal height will
suffice, however, depending on your needs, you will need to adjust this in the
CSS code.

IE 6 - Does not like the default relative source in the CSS for the PNG
overlay image used by the Lightbox JavaScript. I suspect that the base url tag
is interfering with it, even though technically it should not. To fix this I
have assigned an absolute path on CSS "filter" attribute in the (* html
#overlay) declaration in my pagestyles.css file. This absolute path assumes
you are running your Drupal site from the root of your webserver. If not, and
you want the PNG overlay to work in IE 6, please set the correct "src" url in
the "filter" declaration.

IE 6 - In conjunction with the pngfix JavaScript, collapses all PNG margins
this has some minor formatting problems in the admin watchdog section. So
watch out for this in your content areas too.

Opera 8.5 - Does not paint absolution positioned DIVs that are explicitly
specified to be 100% height even when their parent container has this
specified too. I was told by Opera that this should be fixed by the next major
version. Until then, you will see the following visual bugs. - Left shadow of
page content only shows to min-height of 1000px - Right background of
navigation/menus shows to min-height of 1000px - Comment boxes are slightly
off.

The site mission is not showing in node or admin pages, maybe a feature?


|   Things On Top Of My To Do List:
 `-------------------------------------------------------------------------+

Set where a user defined logo would go. Perhaps another layer in the masthead
or perhaps replace the MetaTools instructions with image. If so, this image
would have to be a PNG.

 - Theme where avatar images would appear, and posts. Perhaps gravatar.

I would love to have some JS hide/show menus on the right. Especially, for the
"administer" menu items so that you would not have to scroll so much. Perhaps
working with the "nicemenu" module and some custom JS?

It would be need if some PHP or JavaScript code was on the page and if
JavaScript was disabled, it would first, remove the MetaTools DIV and second,
replace it with a nicely styled message that says to enable JavaScript.

| Things On The Bottom Of My To Do List: (random order)
`-------------------------------------------------------------------------+

Test forums and polls theme/override drupal.css to match meta design. Maybe
something like http://www.spinspy.com/

Add a print style sheet to remove web formating.

I would very much like to theme the navigation menu if it was to appear in
"header" content areas vs. the right or left side bar. I think this would help
administration since the list items for this are quite long. But to do this, I
would have to write some logic (I Dont Know PHP) that would test to see where
the navigation is being asked to appear and if in the "header" change it's
output. I know I can do this with template.php, but off hand I'm sure I can do
the conditional PHP work.

Right now the status messages only work with the Fade Any Thing (FAT)
JavaScript when messages appear on a page with the default 'drupal.js' in the
head, for example, submit content page & settings page. The FAT JavaScript
does not seem to work when changing a theme page in account or admin settings.
My assumption is due to some onload commands that need to be shuffled around.

Add JavaScript handlers to read the URL for which node you are in and change
the 2 sliding door images in the primary navigation to their on state. Note, I
could not do this for normal a:hover in CSS since their it did not work
because it was behind another absolute positioned DIV and it needed to swap
both images of the sliding door, again requiring JavaScript.

Possibly theme the output of the watchdog module so that I can specifically
theme it's table without interfering with tables that users may post into
their blogs.


