<?php

// Uncomment this if you are not using the lightbox.js in the page.tpl.php file.
// drupal_set_html_head('<script type="text/javascript" src="themes/meta/js/lightbox.js"></script>');

$size = _image_get_dimensions('thumbnail');
$width = $size['width'];
$height = $size['height'];


$content = '';
if (count($galleries)) {
  $content.= "\n".'<div id="galleries">'."\n";
  $content.= "\t".'<ul>'."\n";
  foreach ($galleries as $gallery) {
    // Finding out the width of an image being used. Sould be 90x68 or 68x90 pixels.
    $metaglryimg = image_get_info(file_create_path($gallery->latest->images['thumbnail']));
    $metaglryimgwdth = $metaglryimg[width];
    // Start setting the mood...
    if ($gallery->count)
      $content .= "\t"."\t".'<li class="clearfix">'."\n";
      $content .= "\t"."\t"."\t".'<div class="preview">';
      // Is it horizontal?
      if ($metaglryimgwdth=='90') {
        $content.= '<span class="horiz">';
      }
      // Is it vertical? Or to be safe, not horizontal?
      if ($metaglryimgwdth!='90') {
        $content.= '<span class="vert">';
      }
      $content.= l(image_display($gallery->latest, 'thumbnail'), 'image/tid/'.$gallery->tid, array(), NULL, NULL, FALSE, TRUE);
      $content.= "</span>"."</div>"."\n";
      $content.= "\t"."\t"."\t".'<div class="preview-desc">'."\n";
      $content.= "\t"."\t"."\t"."\t"."<h2>".l($gallery->name, 'image/tid/'.$gallery->tid) . "</h2>\n";
      $content.= "\t"."\t"."\t"."\t".'<div class="description">'. check_markup($gallery->description) ."</div>\n";
      $content.= "\t"."\t"."\t"."\t".'<span class="count">' . format_plural($gallery->count, 'There is 1 image in this gallery', 'There are %count images in this gallery') . "</span>".'<br />'."\n";
      if ($gallery->latest->changed) {
        $content.= "\t"."\t"."\t"."\t".'<span class="last">'. t('Last updated: %date', array('%date' => format_date($gallery->latest->changed))) . "</span>".'<br />'."\n";
      }
      $content.= "\t"."\t"."\t"."\t".'<span class="view-button">'.l('<img src="themes/meta/images/gallery-viewbutton.gif" alt="View Gallery" title="View This Gallery" />', 'image/tid/'.$gallery->tid, array(), NULL, NULL, FALSE, TRUE) .'</span>'.'<br />'."\n";
      $content.= "\t"."\t"."\t"."</div>\n";
      $content.= "\t"."\t"."</li>\n";
    }
  $content.= "\t"."</ul>\n";
  $content.= "</div>\n";
}


if (count($images)) {
  $content.= "\n".'<div id="gallery">'."\n";
  $content.= "\t".'<ul>'."\n";
  foreach ($images as $image) {
    // Finding out the width of an image being used. Sould be 90x68 or 68x90 pixels.
    $metaimg = image_get_info(file_create_path($image->images['thumbnail']));
    $metaimgwdth = $metaimg[width];
    // Start setting the mood...
    $content.= "\t"."\t"."<li";
    if ($image->sticky) {
      $content.= ' class="sticky"';
    }
    $content.= ">\n";
    // If you want to go to the node and not use lightbox, use this line instead of the one below.
    /* $content .= "\t"."\t"."\t".'<h3>'.l($image->title, 'node/'.$image->nid)."</h3>"."\n"; */
    // If you want to go to the image using lightbox, use this line instead of the one above.
    $content.= "\t"."\t"."\t".'<h3>'.l($image->title, 'image/view/'.$image->nid, array('rel' => 'lightbox[gallery]', 'title' => $image->body))."</h3>"."\n";
    $content.= "\t"."\t"."\t";
    // Is it horizontal?
    if ($metaimgwdth=='90') {
      $content.= '<span class="horiz">';
    }
    // Is it vertical? Or to be safe, not horizontal?
    if ($metaimgwdth!='90') {
      $content.= '<span class="vert">';
    }
    // If you want to go to the node and not use lightbox, use this line instead of the one below.
    /* $content .= l(image_display($image, 'thumbnail'), 'node/'.$image->nid, array(), NULL, NULL, FALSE, TRUE); */
    // If you want to go to the image using lightbox, use this line instead of the one above.
    $content.= l(image_display($image, 'thumbnail'), 'image/view/'.$image->nid, array('rel' => 'lightbox[gallery]', 'title' => $image->body), NULL, NULL, FALSE, TRUE);
    $content.= "</span>";
    $content.= "\n";
    if (theme_get_setting('toggle_node_info_' . $image->type)) {
      $content.= "\t"."\t"."\t".'<div class="date">'.format_date($image->created, 'small')."</div>\n";
    }
    $content.= "\t"."\t"."</li>\n";
  }
  $content.= "\t"."</ul>\n";
  $content.= "</div>\n";
}


if ($pager = theme('pager', NULL, variable_get('image_images_per_page', 6), 0)) {
  $content.= $pager;
}

If (count($images) + count($galleries) == 0) {
    $content.= '<p class="count">' . format_plural(0, 'There is 1 image in this gallery', 'There are %count images in this gallery') . "</p>\n";
}

print $content;


?>
 