<div class="comment-box">

  <h3 class="title"><?php print $title ?></h3>
  <div class="comment-info"><?php print $author ?>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<?php print $date ?></div>
  
  <div class="comment <?php print ($comment->new) ? 'comment-new' : '' ?>"> 
    <div class="tl"><div class="tr"><div class="br"><div class="bl">
    
      <div class="comment-vertspacer"></div>

      <?php if ($picture) : ?>
        <div class="picture"><?php print $picture ?></div>
      <?php endif; ?>
    
      <div class="content"><?php print $content ?></div>
      
      <div class="comment-links-box"><span class="comment-links"><?php print $links ?></span></div>
    
    </div></div></div></div>
  </div>

</div>
