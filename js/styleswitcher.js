

// ------------------------------------------------------------------------
// |  MetaTools (preloader for BODY Background only)                      |
// ------------------------------------------------------------------------

if (document.images) {
  preload_image_object = new Image();
  image_url = new Array();
  image_url[0] = "themes/meta/css/images/background-green.gif";
  image_url[1] = "themes/meta/css/images/background-paper.gif";
  image_url[2] = "themes/meta/css/images/background-grey.gif";
  image_url[3] = "themes/meta/css/images/background-pink.gif";
  image_url[4] = "themes/meta/css/images/background-black.gif";
    var i = 0;
    for(i=0; i<=4; i++) 
      preload_image_object.src = image_url[i];
}

// ------------------------------------------------------------------------
// |  MetaTools (user WIDTH functions)                                    |
// ------------------------------------------------------------------------

function setActiveUserWidth(userwdthid) {
  var i, a;
  for(i=0; (a = document.getElementsByTagName("link")[i]); i++) {
    if(a.getAttribute("title") == "userdefwdth") {
      a.disabled = true;
      if(a.getAttribute("id") == userwdthid) a.disabled = false;
    }
  }
}

function getActiveUserWidth() {
  var i, a;
  for(i=0; (a = document.getElementsByTagName("link")[i]); i++) {
    if(a.getAttribute("title") == "userdefwdth" && !a.disabled)
      return a.getAttribute("id");
  }
  return null;
}

// ------------------------------------------------------------------------
// |  MetaTools (user FONTSIZE functions)                                 |
// ------------------------------------------------------------------------

function setActiveUserFontSz(userfontszid) {
  var i, a;
  for(i=0; (a = document.getElementsByTagName("link")[i]); i++) {
    if(a.getAttribute("title") == "userdeffontsz") {
      a.disabled = true;
      if(a.getAttribute("id") == userfontszid) a.disabled = false;
    }
  }
}

function getActiveUserFontSz() {
  var i, a;
  for(i=0; (a = document.getElementsByTagName("link")[i]); i++) {
    if(a.getAttribute("title") == "userdeffontsz" && !a.disabled)
      return a.getAttribute("id");
  }
  return null;
}

// ------------------------------------------------------------------------
// |  MetaTools (user BACKGROUND functions)                               |
// ------------------------------------------------------------------------

function setActiveUserBackground(userbgid) {
  var i, a;
  for(i=0; (a = document.getElementsByTagName("link")[i]); i++) {
    if(a.getAttribute("title") == "userdefbg") {
      a.disabled = true;
      if(a.getAttribute("id") == userbgid) a.disabled = false;
    }
  }
}

function getActiveUserBackground() {
  var i, a;
  for(i=0; (a = document.getElementsByTagName("link")[i]); i++) {
    if(a.getAttribute("title") == "userdefbg" && !a.disabled) 
      return a.getAttribute("id");
  }
  return null;
}

// ------------------------------------------------------------------------
// |  MetaTools (simple COOKIE functions)                                 |
// ------------------------------------------------------------------------

function createCookie(name,value,days) {
  if (days) {
    var date = new Date();
    date.setTime(date.getTime()+(days*24*60*60*1000));
    var expires = "; expires="+date.toGMTString();
  }
  else expires = "";
  document.cookie = name+"="+value+expires+"; path=/";
}

function readCookie(name) {
  var nameEQ = name + "=";
  var ca = document.cookie.split(';');
  for(var i=0;i < ca.length;i++) {
    var c = ca[i];
    while (c.charAt(0)==' ') c = c.substring(1,c.length);
    if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
  }
  return null;
}

// ------------------------------------------------------------------------
// |  MetaTools (global VARs and actions)                                 |
// ------------------------------------------------------------------------

window.onunload = function(e) {
  var userwdthid = getActiveUserWidth();
  var userfontszid = getActiveUserFontSz();
  var userbgid = getActiveUserBackground();
  createCookie("userwdth", userwdthid, 365);
  createCookie("userfontsz", userfontszid, 365);
  createCookie("userbg", userbgid, 365);
}

var userwdthid = readCookie("userwdth");
setActiveUserWidth(userwdthid);

var userfontszid = readCookie("userfontsz");
setActiveUserFontSz(userfontszid);

var userbgid = readCookie("userbg");
setActiveUserBackground(userbgid);

