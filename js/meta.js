

// Drupal.js Global Killswitch
if (isJsEnabled()) {
  addLoadEvent(externalLinks);
  addLoadEvent(replaceSearchField);
}

	
// ------------------------------------------------------------------------
// |  XHTML Strict (rel="external" to target="_blank" function)           |
// |  http://www.sitepoint.com/article/standards-compliant-world/         |
// ------------------------------------------------------------------------

function externalLinks() { 
 var anchors = document.getElementsByTagName("a"); 
 for (var i=0; i<anchors.length; i++) { 
   var anchor = anchors[i]; 
   if (anchor.getAttribute("href") && 
       anchor.getAttribute("rel") == "external") 
       anchor.target = "_blank"; 
 } 
} 


// ------------------------------------------------------------------------
// |  Safari Styled - MetaTool Search Button                              |
// ------------------------------------------------------------------------

function addSearchEvent(obj, evType, fn, useCapture){
  if (obj.addEventListener){
    obj.addEventListener(evType, fn, useCapture);
    return true;
  } else if (obj.attachEvent){
    var r = obj.attachEvent("on"+evType, fn);
    return r;
  } else {
    alert("Handler could not be attached");
  }
}

function removeSearchEvent(obj, evType, fn, useCapturepture){
  if (obj.removeEventListener){
    obj.removeEventListener(evType, fn, useCapture);
    return true;
  } else if (obj.detachEvent){
    var r = obj.detachEvent("on"+evType, fn);
    return r;
  } else {
    alert("Handler could not be removed");
  }
}

var DEF_VAL   = "search...";
var isSafari  = ((parseInt(navigator.productSub)>=20020000)&& 
                (navigator.vendor.indexOf("Apple Computer")!=-1));

function replaceSearchField() {
  var searchField = document.getElementById('edit-search_theme_form_keys');
  if (isSafari) {
    searchField.setAttribute('type', 'search');
    searchField.setAttribute('placeholder', DEF_VAL);
    searchField.setAttribute('size', '22');
  } else {
    addSearchEvent(searchField, 'focus', focusSearch, false);
    addSearchEvent(searchField, 'blur',  blurSearch, false);
    if (searchField.value=='') searchField.value = DEF_VAL;
    }
}

function focusSearch() {
  if (this.value==DEF_VAL) {
    this.value = '';
    this.setAttribute('class', 'focus');
  }
}

function blurSearch() {
  if (this.value=='') {
    this.value = DEF_VAL;
    this.removeAttribute('class', 'focus');
  }
}


// ------------------------------------------------------------------------
// |  Fade Anything Technique (messages)                                  |
// ------------------------------------------------------------------------
// @name      The Fade Anything Technique
// @namespace http://www.axentric.com/aside/fat/
// @version   1.0-RC1
// @author    Adam Michela

var Fat = {
	make_hex : function (r,g,b) 
	{
		r = r.toString(16); if (r.length == 1) r = '0' + r;
		g = g.toString(16); if (g.length == 1) g = '0' + g;
		b = b.toString(16); if (b.length == 1) b = '0' + b;
		return "#" + r + g + b;
	},
	fade_all : function ()
	{
		var a = document.getElementsByTagName("*");
		for (var i = 0; i < a.length; i++) 
		{
			var o = a[i];
			var r = /fade-?(\w{3,6})?/.exec(o.className);
			if (r)
			{
				if (!r[1]) r[1] = "";
				if (o.id) Fat.fade_element(o.id,null,null,"#"+r[1]);
			}
		}
	},
	fade_element : function (id, fps, duration, from, to) 
	{
		if (!fps) fps = 30;
		if (!duration) duration = 3000;
		if (!from || from=="#") from = "#FFFF33";
		if (!to) to = this.get_bgcolor(id);
		
		var frames = Math.round(fps * (duration / 1000));
		var interval = duration / frames;
		var delay = interval;
		var frame = 0;
		
		if (from.length < 7) from += from.substr(1,3);
		if (to.length < 7) to += to.substr(1,3);
		
		var rf = parseInt(from.substr(1,2),16);
		var gf = parseInt(from.substr(3,2),16);
		var bf = parseInt(from.substr(5,2),16);
		var rt = parseInt(to.substr(1,2),16);
		var gt = parseInt(to.substr(3,2),16);
		var bt = parseInt(to.substr(5,2),16);
		
		var r,g,b,h;
		while (frame < frames)
		{
			r = Math.floor(rf * ((frames-frame)/frames) + rt * (frame/frames));
			g = Math.floor(gf * ((frames-frame)/frames) + gt * (frame/frames));
			b = Math.floor(bf * ((frames-frame)/frames) + bt * (frame/frames));
			h = this.make_hex(r,g,b);
		
			setTimeout("Fat.set_bgcolor('"+id+"','"+h+"')", delay);

			frame++;
			delay = interval * frame; 
		}
		setTimeout("Fat.set_bgcolor('"+id+"','"+to+"')", delay);
	},
	set_bgcolor : function (id, c)
	{
		var o = document.getElementById(id);
		o.style.backgroundColor = c;
	},
	get_bgcolor : function (id)
	{
		var o = document.getElementById(id);
		while(o)
		{
			var c;
			if (window.getComputedStyle) c = window.getComputedStyle(o,null).getPropertyValue("background-color");
			if (o.currentStyle) c = o.currentStyle.backgroundColor;
			if ((c != "" && c != "transparent") || o.tagName == "BODY") { break; }
			o = o.parentNode;
		}
		if (c == undefined || c == "" || c == "transparent") c = "#FFFFFF";
		var rgb = c.match(/rgb\s*\(\s*(\d{1,3})\s*,\s*(\d{1,3})\s*,\s*(\d{1,3})\s*\)/);
		if (rgb) c = this.make_hex(parseInt(rgb[1]),parseInt(rgb[2]),parseInt(rgb[3]));
		return c;
	}
}

if (isJsEnabled()) {
  addLoadEvent(Fat.fade_all);
}
