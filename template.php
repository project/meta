<?php


/**
 * I need the 'drupal.js' file on every page to use it. I had first tried:
 * drupal_set_html_head('<script type="text/javascript" src="misc/drupal.js"></script>');
 * But that duplicated resuts on some pages. Now I am just using this.
 */

drupal_add_js('misc/drupal.js');


/**
 * Catch the theme_image_gallery function, redirect through the template api
 * and point Drupal to the "image_gallery.tpl.php" file to determine the layout
 * of your image gallery pages.
 */

function phptemplate_image_gallery($galleries, $images) {
  return _phptemplate_callback('image_gallery', array('galleries' => $galleries, 'images' => $images));
}


/**
 * Catch the theme_status_messages function in "includes/theme.inc", and output
 * directly from template.php. Restyling this a bit to work with the FAT js function
 * and I've even added an if else so that we can style 'error' differently from 'status'.
 */

function phptemplate_status_messages() {
  if ($data = drupal_get_messages()) {
    $output = '';
    foreach ($data as $type => $messages) {
      if ($type == 'error') {
        $output .= "<br />\n<div id=\"content-message\" class=\"messages error fade-ffde00\">\n";
      }
      else if ($type == 'status') {
        $output .= "<br />\n<div id=\"content-message\" class=\"messages status fade-78ff00\">\n";
      }
      if (count($messages) > 1) {
        $output .= " <ul>\n";
        foreach($messages as $message) {
          $output .= '  <li>'. $message ."</li>\n";
        }
        $output .= " </ul>\n";
      }
      else {
        $output .= $messages[0];
      }
      $output .= "</div>\n";
    }
    return $output;
  }
}


/**
 * Catch the theme_maintenance_page function in "includes/theme.inc", and redirect
 * through the template api and point Drupal to the "maintenance_page.tpl.php" file
 # to style the 503 Service Unavailable page when the site is offline. The phptemplate
 * variables function is used to get the $stie_name variable used on the page template.
 */

function phptemplate_maintenance_page($content) {
  return _phptemplate_callback('maintenance_page', array('content' => $content));
}

function _phptemplate_variables($hook, $vars) {
  switch($hook) {
    case 'maintenance_page' :
      $vars['site_name'] = variable_get('site_name', '') ;
    break;
  }
  return $vars;
}


/**
 * Catch the theme_comment_thread_expanded function in "modules/comment.module", and output
 * directly from template.php. I have added some logic that will set the maximum indentation
 * at 9 levels deep. I have also reduced the indentation from Drupal's 25px to 15px. I think
 * the combination of these two helps keep things looking clean. If you want to change that
 * maximum depth indentation on the 3rd line, do not forget to also take what ever number you
 * enter in there and multiple it by 15 and enter that value into the else's margin-left
 * inline pixel declaration too.
 */ 

function phptemplate_comment_thread_expanded($comment) {
  $output = '';
  if ($comment->depth <= 9) {
    $output .= "<div class=\"comment-box_indent\" style=\"margin-left:". ($comment->depth * 15) ."px;\">\n";
  }
  else {
    $output .= "<div class=\"comment-box_indent\" style=\"margin-left: 135px\">\n";
  }
  $output .= theme('comment_view', $comment, module_invoke_all('link', 'comment', $comment, 0));
  $output .= "</div>\n";
  return $output;
}





?>