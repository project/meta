

  First, thanks for downloading my theme contribution for Drupal 4.7, my first
theme attempt using PHPTemplate. I have developed this theme so that I can help
give back to the Drupal community by providing a unique (non-ported) personal
blog option for first time Drupal user. Likewise, while utilizing as many hooks
into the Drupal core, themeing engine, and popular modules, it is my hopes that
I can help others learn how powerful and extensible the Drupal system is. The
theme also passes XHTML Strict validation. To maintain that, I suggest you read 
through the conventions (link below) the Meta theme offers.


|   ABOUT THE META THEME:
 `-------------------------------------------------------------------------+

To ease the documentation of the Meta theme, I have placed all feature 
examples, documentation, and useful information on the demonstration site 
located at:

  http://www.metaskills.net/meta-theme-for-drupal/

The most common documentation and feature links are:

  Installation:
  http://www.metaskills.net/meta-theme-for-drupal/installation-assumptions

  Image Gallery Settings:
  http://www.metaskills.net/meta-theme-for-drupal/image-gallery-theme-and-the-lightbox-javascript

    * Images uploaded are in a 4:3 (or 3:4) aspect ration. 
    * These settings and pixel sizes in (administer � settings � image)
      * Thumbnail: (90) Width (90) Height
      * Preview: (523) Width (523) Height
      * Optional: (12) Images per page

  Customization:
  http://www.metaskills.net/meta-theme-for-drupal/options-and-customization

  Photo Fancy Border Code Snippet Examples:
  http://www.metaskills.net/meta-theme-for-drupal/photo-fancy-border-code-snippet-examples



|   CREDITS:
`-------------------------------------------------------------------------+

The Meta theme was developed by Ken Collins after many long hours of CSS, PHP
and JavaScript work. You can find out more about what I do at
http://www.metaskills.net/ other credits, ideas, concepts include the following.

Styleswitcher for MetaTools - Concept by Paul Sowden
  http://www.alistapart.com/articles/alternate/
  http://www.idontsmoke.co.uk/

dp.SyntaxHighlighter - Courtesy of Alex Gorbatchev
  http://www.dreamprojections.com/syntaxhighlighter/

PNG Support for IE 6 - Courtesy of Bob Osola
  http://homepage.ntlworld.com/bobosola

Photo Gallery Concept and Inspiration - Concept by Douglas Bowman
  http://stopdesign.com/templates/photos/

Image Lightbox Feature - By Lokesh Dhakar
  http://www.huddletogether.com/projects/lightbox/



