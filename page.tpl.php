<?php echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" 
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="<?php print $language ?>">
<head>
<title><?php print $head_title ?></title>
  <meta name="generator" content="Drupal" />
  <?php print $head ?>
  <?php print "$styles\n"?>
  <link rel="stylesheet" type="text/css" media="screen, projection" href="<?php print base_path() . path_to_theme() ?>/css/userstyle-wdth_liquid.css" title="userdefwdth" class="userwdth" id="userwdth-liquid" />
  <link rel="stylesheet" type="text/css" media="screen, projection" href="<?php print base_path() . path_to_theme() ?>/css/userstyle-wdth_fixed.css" title="userdefwdth" class="userwdth" id="userwdth-fixed" />
  <link rel="stylesheet" type="text/css" media="screen, projection" href="<?php print base_path() . path_to_theme() ?>/css/userstyle-fnt_large.css" title="userdeffontsz" class="userfontsz" id="userfontsz-large" />
  <link rel="stylesheet" type="text/css" media="screen, projection" href="<?php print base_path() . path_to_theme() ?>/css/userstyle-fnt_normal.css" title="userdeffontsz" class="userfontsz" id="userfontsz-normal" />
  <link rel="stylesheet" type="text/css" media="screen, projection" href="<?php print base_path() . path_to_theme() ?>/css/userstyle-bg_black.css" title="userdefbg" class="userbg" id="userbg-black" />
  <link rel="stylesheet" type="text/css" media="screen, projection" href="<?php print base_path() . path_to_theme() ?>/css/userstyle-bg_pink.css" title="userdefbg" class="userbg" id="userbg-pink" />
  <link rel="stylesheet" type="text/css" media="screen, projection" href="<?php print base_path() . path_to_theme() ?>/css/userstyle-bg_grey.css" title="userdefbg" class="userbg" id="userbg-grey" />
  <link rel="stylesheet" type="text/css" media="screen, projection" href="<?php print base_path() . path_to_theme() ?>/css/userstyle-bg_paper.css" title="userdefbg" class="userbg" id="userbg-paper" />
  <link rel="stylesheet" type="text/css" media="screen, projection" href="<?php print base_path() . path_to_theme() ?>/css/userstyle-bg_green.css" title="userdefbg" class="userbg" id="userbg-green" />
  <style type="text/css" media="all">@import "<?php print base_path() . path_to_theme() ?>/css/SyntaxHighlighter.css";</style>
  <script type="text/javascript" src="<?php print base_path() . path_to_theme() ?>/js/meta.js"></script>
  <script type="text/javascript" src="<?php print base_path() . path_to_theme() ?>/js/styleswitcher.js"></script>
  <script type="text/javascript" src="<?php print base_path() . path_to_theme() ?>/js/prototype.js"></script>
  <script type="text/javascript" src="<?php print base_path() . path_to_theme() ?>/js/scriptaculous.js?load=effects"></script>
  <script type="text/javascript" src="<?php print base_path() . path_to_theme() ?>/js/lightbox.js"></script>
  <!--[if lt IE 7]>
  <style type="text/css" media="all">@import "<?php print base_path() . path_to_theme() ?>/css/hacks-ie6-win.css";</style>
  <script type="text/javascript" src="<?php print base_path() . path_to_theme() ?>/js/pngfix.js"></script>
  <![endif]-->
  <!--[if IE 7]>
  <style type="text/css" media="all">@import "<?php print base_path() . path_to_theme() ?>/css/hacks-ie7-win.css";</style>
  <![endif]-->
</head>

<body id="body-section" <?php print $onload_attributes ?>>

<div id="metatools-bounding1">
  <div id="metatools-bounding2">
    
    <div id="metatools-bg_box">
      <div id="metatools-bg_left"></div>
      <div id="metatools-bg_middle"></div>
      <div id="metatools-bg_right"></div>
    </div>
    
    <div id="metatoolbox">
      <span id="metatool-title"></span>
      <span class="metatool-sepline"></span>
      <span id="metatool-width_liquid" onclick="setActiveUserWidth('userwdth-liquid'); return false;"></span>
      <span id="metatool-width_fixed" onclick="setActiveUserWidth('userwdth-fixed'); return false;"></span>
      <span class="metatool-sepline"></span>
      <span id="metatool-fontsize_smaller" onclick="setActiveUserFontSz('userfontsz-normal'); return false;"></span>
      <span id="metatool-fontsize_bigger" onclick="setActiveUserFontSz('userfontsz-large'); return false;"></span>
      <span class="metatool-sepline"></span>
      <span id="metatool-bg_green" onclick="setActiveUserBackground('userbg-green'); return false;"></span>
      <span id="metatool-bg_paper" onclick="setActiveUserBackground('userbg-paper'); return false;"></span>
      <span id="metatool-bg_grey" onclick="setActiveUserBackground('userbg-grey'); return false;"></span>
      <span id="metatool-bg_pink" onclick="setActiveUserBackground('userbg-pink'); return false;"></span>
      <span id="metatool-bg_black" onclick="setActiveUserBackground('userbg-black'); return false;"></span>
      <span class="metatool-sepline"></span>
      <div id="metatool-search">
        <?php print $search_box ?>
      </div>
    </div>
    
  </div><!-- #metatools-bounding2 -->
</div><!-- #metatools-bounding1 -->
<div id="metatools-bottom_spacing"></div>

<div id="masterbounding1">
  <div id="masterbounding2">
  
  <div id="masthead-border">
    <div id="masthead-border_left"></div>
    <div id="masthead-border_top"></div>
    <div id="masthead-border_right"></div>
  </div>
  <div id="masthead-imageshdws_box">
    <div id="masthead-imageshdws_left"></div>
    <div id="masthead-imageshdws_right"></div>
    <div id="masthead-imageshdws_middle"></div>
  </div>
  <div id="masthead-imageshdws-tt_box">
    <div id="masthead-imageshdws-tt_left"></div>
    <div id="masthead-imageshdws-tt_right"></div>
    <div id="masthead-imageshdws-tt_middle"></div>
  </div>
  <div id="navigation-top">
    <?php if (count($primary_links)) : ?>
    <ul id="primary">
      <?php foreach ($primary_links as $link): ?>
      <li><?php print $link?></li>
      <?php endforeach; ?>
    </ul>
    <?php endif; ?>
  </div>
  <div id="masthead-sitename_box">
    <div id="masthead-sitename"><?php if ($site_name) { ?><?php print $site_name ?><?php } ?></div>
  </div>
  <div id="masthead-image"></div>
  
  <div id="contentbgtop-bounding">
    <div id="contentbgtop-left"></div>
    <div id="contentbgtop-middle"></div>
    <div id="contentbgtop-right"></div>
  </div>
  
  <div id="contentarea-bounding">
    <div id="contentarea-left"></div>
    
    <div id="contentarea-middle1">
      <div id="contentarea-middle2">
        <!-- BEGIN - Main Content Area -->
                  
          <?php print $breadcrumb ?>
          
          <?php if ($messages != ""): ?>
            <?php print $messages ?>
          <?php endif; ?>
          
          <?php if ($header != ""): ?>
            <div id="content-header"><?php print $header ?></div>
          <?php endif; ?>
          
          <?php if ($help != ""): ?>
            <p id="content-help"><?php print $help ?></p>
          <?php endif; ?>
          
          <?php if ($title != ""): ?>
            <h1 class="content-title"><?php print $title ?></h1>
          <?php endif; ?>
          
          <?php if ($tabs != ""): ?>
            <?php print $tabs ?>
          <?php endif; ?>
          
          <?php print($content) ?>
          
        <!-- END - Main Content Area -->
      </div><!-- #contentarea-middle2 -->
    </div><!-- #contentarea-middle1 -->
    
    <div id="contentarea-right1">
      <div id="contentarea-right2">
        <!-- BEGIN - Left & Right Content Area -->
        <?php if ($sidebar_left != ""): ?>
        <?php print $sidebar_left ?>
        <?php endif; ?>
        <?php if ($sidebar_right != ""): ?>
        <?php print $sidebar_right ?>
        <?php endif; ?>
        <!-- END - Left & Right Content Area -->
      </div><!-- #contentarea-right2 -->
    </div><!-- #contentarea-right1 -->
    
  </div><!-- #contentarea-bounding -->
  
  <div id="contentbgbottom-bounding">
    <div id="contentbgbottom-left"></div>
    <div id="contentbgbottom-middle"></div>
    <div id="contentbgbottom-right"></div>
  </div>
  
  </div><!-- #masterbounding1 -->
</div><!-- #masterbounding2 -->

<div class="br"><br /></div>

<div id="footer-area">
  <?php if ($site_name) { ?>
    <div class='site-name'><a href="./" title="Home"><?php print $site_name ?></a></div>
  <?php } ?>
  <?php if ($site_slogan) { ?>
    <div class='site-slogan'><?php print $site_slogan ?></div>
  <?php } ?>
  <?php if ($mission) { ?>
    <div class="site-mission"><?php print $mission ?></div>
  <?php } ?>
  <div id="secondary-links">
    <?php if (count($secondary_links)) : ?>
      <?php foreach ($secondary_links as $link): ?>
        <?php print $link?>&nbsp;&nbsp;&nbsp;
      <?php endforeach; ?>
    <?php endif; ?>
  </div>
  <?php if ($footer_message) { ?>
    <div class="br"><br /></div>
    <div class='footer-message'><?php print $footer_message ?></div>
  <?php } ?>
</div>

<div class="br"><br /></div>

<script type="text/javascript" src="<?php print base_path() . path_to_theme() ?>/js/SyntaxHighlighter/shCore.js"></script>
<script type="text/javascript" src="<?php print base_path() . path_to_theme() ?>/js/SyntaxHighlighter/shBrushCSharp.js"></script>
<script type="text/javascript" src="<?php print base_path() . path_to_theme() ?>/js/SyntaxHighlighter/shBrushPhp.js"></script>
<script type="text/javascript" src="<?php print base_path() . path_to_theme() ?>/js/SyntaxHighlighter/shBrushJScript.js"></script>
<script type="text/javascript" src="<?php print base_path() . path_to_theme() ?>/js/SyntaxHighlighter/shBrushVb.js"></script>
<script type="text/javascript" src="<?php print base_path() . path_to_theme() ?>/js/SyntaxHighlighter/shBrushSql.js"></script>
<script type="text/javascript" src="<?php print base_path() . path_to_theme() ?>/js/SyntaxHighlighter/shBrushXml.js"></script>
<script type="text/javascript" src="<?php print base_path() . path_to_theme() ?>/js/SyntaxHighlighter/shBrushDelphi.js"></script>
<script type="text/javascript" src="<?php print base_path() . path_to_theme() ?>/js/SyntaxHighlighter/shBrushPython.js"></script>
<script type="text/javascript">
dp.SyntaxHighlighter.HighlightAll('code');
</script>

<?php print $closure;?>
</body>
</html>